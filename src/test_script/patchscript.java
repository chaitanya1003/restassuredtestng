package test_script;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import common_method.api_trigger;
import common_method.testng_retry_analyser;
import common_method.utilityclass;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class patchscript extends api_trigger {

	File logfolder;
	Response response;
	ResponseBody responseBody;

	@BeforeTest
	public void setup() {
		logfolder = utilityclass.createFolder("Patch_API");

	}

	@Test (retryAnalyzer=testng_retry_analyser.class)

	public void validate() {
		response = patch_api_trigger(patch_request_body(), patch_endpoint());
		int statuscode = response.statusCode();
		responseBody = response.getBody();
		String res_name = responseBody.jsonPath().getString("name");
		String res_job = responseBody.jsonPath().getString("job");
		String res_updatedAt = responseBody.jsonPath().getString("updatedAt");
		res_updatedAt = res_updatedAt.toString().substring(0, 11);

		// step 4 parse the RequestBody using json path extract RequestBody parameter

		JsonPath jsp_req = new JsonPath(patch_request_body());
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		// 4.1: Generate expected Date
		LocalDateTime currentDate = LocalDateTime.now();
		String expecteddate = currentDate.toString().substring(0, 11);
		Assert.assertEquals(statuscode, 201, "correct sttuscode is not found even after retrying for 5 time");

		// step 5 validate using TestNG Assertion
		Assert.assertEquals(res_name, req_name, "Name in ResponseBody is not equal to name sent in RequestBody");
		Assert.assertEquals(res_job, req_job, "Job in ResposneBody is not equal to job sent in RequestBody");
		Assert.assertEquals(res_updatedAt, expecteddate,
				"createdAt is in ResponseBody is not equal to createdAt in request");

	}

	@AfterTest
	public void teardown() throws IOException {
		utilityclass.createLogFile("Patch_API", logfolder, patch_endpoint(), patch_request_body(),
				response.getHeaders().toString(), responseBody.asString());
	}

}
