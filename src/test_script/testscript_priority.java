package test_script;

import java.io.File;
import java.io.IOException;

import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import common_method.api_trigger;
import common_method.testng_retry_analyser;
import common_method.utilityclass;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class testscript_priority extends api_trigger {
	File logfolder;
	Response response;
	ResponseBody responseBody;

	@BeforeTest
	public void setup() {
		logfolder = utilityclass.createFolder("Post_API");

	}

	@Test (priority=1, retryAnalyzer=testng_retry_analyser.class)
	public void validate1() {
		response = post_api_trigger(post_request_body(), post_endpoint());
		int statuscode = response.statusCode();
		responseBody = response.getBody();
		String res_name = responseBody.jsonPath().getString("name");
		String res_job = responseBody.jsonPath().getString("job");
		String res_id = responseBody.jsonPath().getString("id");
		String res_createdAt = responseBody.jsonPath().getString("createdAt");
		res_createdAt = res_createdAt.toString().substring(0, 11);

		// Step 5: Fetch request body parameters
		JsonPath jsp_req = new JsonPath(post_request_body());
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		// Step 6: Generate expected date
		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);
		Assert.assertEquals(statuscode, 201, "Correct status code not found even after retrying for 5 times");
		// Step 6: Validate using TestNG assertions
		Assert.assertEquals(res_name, req_name,
				"Name in reposnseBody is not equal to the name sent in the requestBody");
		Assert.assertEquals(res_job, req_job, "Job in reposnseBody is not equal to the job sent in the requestBody");
		Assert.assertNotNull(res_id, "Id in reposnseBody is found to be Null");
		Assert.assertEquals(res_createdAt, expecteddate, "createdAt in ResponseBody is not equal to Date Generated");
		System.out.println("execute priority1");
	}
	
	@Test (priority=2,retryAnalyzer=testng_retry_analyser.class)
	public void validate2() {
		response = post_api_trigger(post_request_body(), post_endpoint());
		int statuscode = response.statusCode();
		responseBody = response.getBody();
		String res_name = responseBody.jsonPath().getString("name");
		String res_job = responseBody.jsonPath().getString("job");
		String res_id = responseBody.jsonPath().getString("id");
		String res_createdAt = responseBody.jsonPath().getString("createdAt");
		res_createdAt = res_createdAt.toString().substring(0, 11);

		// Step 5: Fetch request body parameters
		JsonPath jsp_req = new JsonPath(post_request_body());
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		// Step 6: Generate expected date
		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);
		Assert.assertEquals(statuscode, 201, "Correct status code not found even after retrying for 5 times");
		// Step 6: Validate using TestNG assertions
		Assert.assertEquals(res_name, req_name,
				"Name in reposnseBody is not equal to the name sent in the requestBody");
		Assert.assertEquals(res_job, req_job, "Job in reposnseBody is not equal to the job sent in the requestBody");
		Assert.assertNotNull(res_id, "Id in reposnseBody is found to be Null");
		Assert.assertEquals(res_createdAt, expecteddate, "createdAt in ResponseBody is not equal to Date Generated");
     System.out.println("execute priority2");
	}

	@AfterTest
	public void teardown() throws IOException {
		utilityclass.createLogFile("post_api_TC1", logfolder, post_endpoint(), post_request_body(),
				response.getHeaders().toString(), responseBody.asString());
	}

}