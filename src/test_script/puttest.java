package test_script;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import common_method.api_trigger;
import common_method.testng_retry_analyser;
import common_method.utilityclass;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class puttest extends api_trigger {

	File logfolder;
	Response response;
	ResponseBody responseBody;

	@BeforeTest
	public void setup() {
		logfolder = utilityclass.createFolder("Put_API");

	}

	@Test (retryAnalyzer=testng_retry_analyser.class)

	public void validate() {
		response = put_api_trigger(put_request_body(), put_endpoint());
		int statuscode = response.statusCode();
		responseBody = response.getBody();
		String res_name = responseBody.jsonPath().getString("name");
		String res_job = responseBody.jsonPath().getString("job");
		String res_updatedAt = responseBody.jsonPath().getString("updatedAt");
		res_updatedAt = res_updatedAt.toString().substring(0, 11);

		// step 3 : parse the requestBody using json path extract the rquestBody
		// parameter
		JsonPath jsp_req = new JsonPath(put_request_body());
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		// step4 : Generate expected Date
		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);
		Assert.assertEquals(statuscode, 201, "correct sttuscode is not found even after retrying for 5 time");

		// step 5 : validate using TestNG Assertion
		Assert.assertEquals(res_name, req_name, "Name in ResponseBody is not equal to name sent in RequestBody");
		Assert.assertEquals(res_job, req_job, "Job in ResposneBody is not equal to job sent in RequestBody");
		Assert.assertEquals(res_updatedAt, expecteddate,
				"createdAt is in ResponseBody is not equal to createdAt in request");

	}

	@AfterTest
	public void teardown() throws IOException {
		utilityclass.createLogFile("put_api_TC1", logfolder, put_endpoint(), put_request_body(),
				response.getHeaders().toString(), responseBody.asString());
	}

}
