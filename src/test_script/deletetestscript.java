package test_script;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import common_method.api_trigger;
import common_method.testng_retry_analyser;
import common_method.utilityclass;
import io.restassured.response.Response;

public class deletetestscript extends api_trigger {

	File logfolder;

	@BeforeTest
	public void setup() {
		logfolder = utilityclass.createFolder("Delete_API");

	}

	@Test (retryAnalyzer=testng_retry_analyser.class)
	public void execute() throws IOException {

		int statuscode = 0;
		for (int i = 0; i < 5; i++) {
			Response response = delete_api_trigger(delete_request_body(), delete_endpoint());
			// step 3 extract the status code
			statuscode = response.statusCode();
			System.out.println(statuscode);
			if (statuscode == 204) {
				utilityclass.createLogFile("Delete_API", logfolder, delete_endpoint(), delete_request_body(),
						response.getHeaders().toString(), response.getBody().asString());
				break;
			} else {
				System.out.println(" status code found in iteration:" + i + "is:" + statuscode
						+ ",and is not equal expected status code hence retrying");
			}

			Assert.assertEquals(statuscode, 200, "correct sttuscode is not found even after retrying for 5 time");
		}
	}
}
