package test_script;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import common_method.api_trigger;
import common_method.testng_retry_analyser;
import common_method.utilityclass;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class getscript extends api_trigger{
	
	File logfolder;

	@BeforeTest
	public void setup() {
		logfolder = utilityclass.createFolder("Get_API");

	}
	@Test (retryAnalyzer=testng_retry_analyser.class)

	public static void execute() throws IOException {
		
		 File logfolder = utilityclass.createFolder("Get_API");
		 int statuscode=0;
		 for (int i=0;i<5;i++) {
			Response response = get_api_trigger(  get_request_body() ,get_endpoint());
			// Step 5: Extract the status code
			  statuscode = response.statusCode();
			System.out.println(statuscode);
			if (statuscode==200) {

			// Step 6: Fetch the ResponseBody parameters
			ResponseBody responseBody = response.getBody();
			System.out.println(responseBody.asString());
			utilityclass.createLogFile("Get_API",logfolder,get_endpoint(),get_request_body(),response.getHeaders().toString(),responseBody.asString()); 
			   validate(responseBody);
		     
		     break;
		    }
		    else {
		    	System.out.println(" status code found in iteration:"+i+"is:"+ statuscode+",and is not equal expected status code hence retrying");
		    }

		    Assert.assertEquals(statuscode,201,"correct sttuscode is not found even after retrying for 5 time");
		    }
	}    
	
		
		public static void validate(ResponseBody responseBody) {
		int exp_page = 2;
		int exp_per_page = 6;
		int exp_total = 12;
		int exp_total_pages = 2;

		// Step 1.2 : Declare expected results of data array
		int[] exp_id = { 7, 8, 9, 10, 11, 12 };
		String[] exp_email = { "michael.lawson@reqres.in", "lindsay.ferguson@reqres.in", "tobias.funke@reqres.in",
				"byron.fields@reqres.in", "george.edwards@reqres.in", "rachel.howell@reqres.in" };
		String[] first_name = {"Michael", "Lindsay", "Tobias","Byron","George","Rachel"};
		String[] last_name = { "Lawson", "Ferguson", "Funke","Fields","Edwards","Howell"};
		String[] avatar = {"https://reqres.in/img/faces/7-image.jpg","https://reqres.in/img/faces/8-image.jpg","https://reqres.in/img/faces/9-image.jpg","https://reqres.in/img/faces/10-image.jpg","https://reqres.in/img/faces/11-image.jpg", "https://reqres.in/img/faces/12-image.jpg"};
		
			

		// Step 1.3 : Declare expected results of support

		String exp_url = "https://reqres.in/#support-heading";
       String exp_text =  "To keep ReqRes free, contributions towards server costs are appreciated!";
       
		// Step 2 : Declare the needed variables
		String hostname = "https://reqres.in";
		String resource = "/api/users?page=2";

		// Step 3: Trigger the API

		// Step 4.1 : Build the request specification using RequestSpecification
		RequestSpecification req_spec = RestAssured.given();

		
		

		// Step 6.1 : Fetch page parameters

		int res_page = responseBody.jsonPath().getInt("page");
		int res_per_page = responseBody.jsonPath().getInt("per_page");
		int res_total = responseBody.jsonPath().getInt("total");
		int res_total_pages = responseBody.jsonPath().getInt("total_pages");

		// Step 6.1: Fetch Size of data array
		List<String> dataArray = responseBody.jsonPath().getList("data");
		int sizeofarray = dataArray.size();

		// Step 7 : Validation

		// Step 7.1 : Validate per page parameters with using TestNG Assertion 
		Assert.assertEquals(res_page, exp_page);
		Assert.assertEquals(res_per_page, exp_per_page);
		Assert.assertEquals(res_total, exp_total);
		Assert.assertEquals(res_total_pages, exp_total_pages);

		// Step 7.2 : Validate data array
		for (int i = 0; i<sizeofarray; i++)
		{
			Assert.assertEquals(Integer.parseInt(responseBody.jsonPath().getString("data[" + i + "].id")), exp_id[i],
					"Validation of id failed for json object at index : " + i);
			Assert.assertEquals(responseBody.jsonPath().getString("data[" + i + "].email"), exp_email[i],
					"Validation of email failed for json object at index : " + i);
       	Assert.assertEquals(responseBody.jsonPath().getString("data["+i+"].first_name"),first_name[i],
       	"validation of first_name failed for json object at index : " +i);
       	Assert.assertEquals(responseBody.jsonPath().getString("data["+i+"].last_name"),last_name[i],
               	"validation of last_name failed for json object at index : " +i);
       	Assert.assertEquals(responseBody.jsonPath().getString("data["+i+"].avatar"),avatar[i],
               	"validation avatar failed for json object at index : " +i);
       			
		}

		// Step 7.3 : Validate support json
		Assert.assertEquals(responseBody.jsonPath().getString("support.url"), exp_url, "Validation of URL failed");
       Assert.assertEquals(responseBody.jsonPath().getString("support.text"),exp_text,"validation of text failed");


	}

}