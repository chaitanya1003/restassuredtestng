package test_script;

import java.io.File;
import java.io.IOException;

import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import common_method.api_trigger;
import common_method.testng_retry_analyser;
import common_method.utilityclass;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class post_param_api extends api_trigger {

	File logfolder;
	Response res;
	ResponseBody responseBody;

	@BeforeTest
	public void setup() {
		logfolder = utilityclass.createFolder("Post_API");

	}

	@Test (retryAnalyzer=testng_retry_analyser.class)

	public void validate() throws IOException {
		res = post_api_trigger(post_param_api("TC_1"), post_endpoint());
		int statuscode = res.statusCode();
		responseBody = res.getBody();
		// Extract the status code
		String res_name = responseBody.jsonPath().getString("name");
		String res_job = responseBody.jsonPath().getString("job");
		String res_id = responseBody.jsonPath().getString("id");
		String res_createdAt = responseBody.jsonPath().getString("createdAt");
		res_createdAt = res_createdAt.toString().substring(0, 11);
		// Fetch the request body parameters
		JsonPath jsp_req = new JsonPath(post_param_api("TC_1"));
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		// Generate expected data.
		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);
		Assert.assertEquals(statuscode, 200, "Correct status code not found even after retrying for 6 times.");
		// Validate using TestNG Assertions
		Assert.assertEquals(res_name, req_name, "Name in ResponseBody is not equal to Name sent in Request Body");
		Assert.assertEquals(res_job, req_job, "Job in ResponseBody is not equal to Job sent in Request Body");
		Assert.assertNotNull(res_id, "Id in ResponseBody is found to be null");
		Assert.assertEquals(res_createdAt, expecteddate, "createdAt in ResponseBody is not equal to Date Generated");
	}

	@AfterTest
	public void teardown() throws IOException {
		utilityclass.createLogFile("post_api_TC_1", logfolder, post_endpoint(), post_request_body(),
				res.getHeaders().toString(), responseBody.asString());
	}
}
